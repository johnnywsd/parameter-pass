


#include <iostream>
using namespace std;

class Foo{
    public:
        int a;
        int* b;
        int c[2];
        int* d;
        Foo();
        ~Foo();
        void show();

};
Foo::Foo()
{
    d = new int[2];
}

Foo::~Foo()
{
    delete[] d;
    d = NULL;

    // Avoid double free error. Please uncomment the code below
    //if(!d)
    //{
        //delete[] d;
        //d = NULL;
    //}
}
void Foo::show()
{
    cout<<"a:"<<a<< endl;
    cout<<"*b:"<<*b<< endl;

    for(int i=0; i<2; i++)
    {
        cout<<"c["<<i<<"]"<<c[i]<<" ";
    }
    cout<<endl;
    for(int i=0; i<2; i++)
    {
        cout<<"*(d+"<<i<<"):"<<*(d+i)<<" ";
    }
    cout<<endl;
}


void bar(Foo foo)
{
    // It do a shallow copy. foo = x
    int bb = 3;
    foo.a = 3;
    foo.b = &bb;
    foo.c[0] = 3;
    foo.c[1] = 3;
    *(foo.d) = 3;
    *(foo.d+1) = 3;

    //foo runs its destructor here. So foo.d is freed. 
    //but foo.d and x.d point to the same space.
    //So at the end of the program there will be double free error.
    //To avoid it. Just make it passing by reference.
}

/****************************************************************************/
//Uncomment the code below. 
//When passing by reference. foo is a alias of x, so it destructor does not run
//void bar(Foo &foo)
//{
    //// It do a shallow copy. foo = x
    //int bb = 3;
    //foo.a = 3;
    //foo.b = &bb;
    //foo.c[0] = 3;
    //foo.c[1] = 3;
    //*(foo.d) = 3;
    //*(foo.d+1) = 3;
//}

/****************************************************************************/

int main(){
    Foo x;
    int bb = 1;
    x.a = 1;
    x.b = &bb;
    x.c[0] = 1;
    x.c[1] = 2;
    *(x.d) = 1;
    *(x.d+1) = 2;


    cout<<"x.show()------------------"<<endl;
    x.show();

    
    cout<<endl<<endl;
    cout<<"After bar(x)------------------"<<endl;
    bar(x);
    x.show();

    cout<<endl<<endl;

    return 0;
}
