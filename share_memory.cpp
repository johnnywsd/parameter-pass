#include <iostream>
using namespace std;

class Foo{
    private:
        int data;
    public:
        int get_data();
        int* get_data_address();
        void set_data(int _data);
};

int Foo::get_data(){
    return data;
}

int* Foo::get_data_address(){
    return &data;
}

void Foo::set_data(int _data){
    this->data = _data;
}

int main(){
    Foo foo;
    //foo.set_data(20);
    int* a = foo.get_data_address();
    *a = 10;
    cout<<foo.get_data()<<endl;
    
    Foo* foo_p = new Foo();
    int* a_p = foo_p->get_data_address();
    *a_p = 20;
    cout<<foo_p->get_data()<<endl;



    return 0;
}
