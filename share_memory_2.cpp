#include <iostream>
using namespace std;

class Foo{
    private:
        int* data;
    public:
        Foo();
        ~Foo();
        int get_data();
        int* get_data_address();
        void set_data(int _data);
};
Foo::Foo(){
    data = new int();
}

Foo::~Foo(){
    delete data;
}

int Foo::get_data(){
    return *data;
}

int* Foo::get_data_address(){
    return data;
}

void Foo::set_data(int _data){
    *(this->data) = _data;
}

int main(){
    
    Foo* foo_p = new Foo();
    int* a_p = foo_p->get_data_address();
    *a_p = 20;
    cout<<foo_p->get_data()<<endl;

    return 0;
}
