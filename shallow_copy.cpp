#include <iostream>
using namespace std;

class Foo{
    public:
        int a;
        int* b;
        int c[2];
        int* d;
        Foo();
        ~Foo();
        void show();

};
Foo::Foo()
{
    d = new int[2];
}

Foo::~Foo()
{
    //delete[] d;
    //d = NULL;

    // Avoid double free error.
    if(!d)
    {
        delete[] d;
        d = NULL;
    }
}
void Foo::show()
{
    cout<<"a:"<<a<< endl;
    cout<<"*b:"<<*b<< endl;

    for(int i=0; i<2; i++)
    {
        cout<<"c["<<i<<"]"<<c[i]<<" ";
    }
    cout<<endl;
    for(int i=0; i<2; i++)
    {
        cout<<"*(d+"<<i<<"):"<<*(d+i)<<" ";
    }
    cout<<endl;
}



int main(){
    Foo x;
    Foo y;
    int bb = 1;
    int gg = 2;
    x.a = 1;
    x.b = &bb;
    x.c[0] = 1;
    x.c[1] = 2;
    *(x.d) = 1;
    *(x.d+1) = 2;


    y = x;
    cout<<"x.show()------------------"<<endl;
    x.show();
    cout<<"y.show()------------------"<<endl;
    y.show();
    cout<<endl<<endl;
    y.a = 9;
    *y.b = 9;
    y.c[0] = 9;
    y.c[1] = 10;
    *(y.d) = 9;
    *(y.d+1) = 10;


    cout<<"x.show()------------------"<<endl;
    x.show();
    cout<<"y.show()------------------"<<endl;
    y.show();       //Double Free Error. d has been freed twice. 
                    //y.d and x.d point to the same space.

    return 0;
}

