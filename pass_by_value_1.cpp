#include<iostream>
using namespace std;

//void foo(int* p)
//{
    //p++;
//}

//Try this.
void foo(int* &p)
{
    p++;
}

int main()
{
    int N = 5;
    int* a =  new int[N];
    for(int i=0; i<N; i++)
    {
        *(a+i) = i;
    }

    for(int i=0; i<N; i++)
    {
        cout<<*(a+i)<<" ";
    }
    cout<<endl; 

    cout<<*a<<endl;

    foo(a);

    cout<<*a<<endl;

    delete[] a;

    //delete[] --a;
}
